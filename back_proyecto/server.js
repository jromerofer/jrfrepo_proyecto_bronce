//APITechU JRF

const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser =require('body-parser'); // libreria para utilizar body en la API

app.use(bodyParser.json());
app.listen(port); // Levantamos el servidor para escuchar en un puerto
console.log("API escuchando en el puerto: " + port + " BBBEEEEEEPPPPPPP");

const userController = require('./controller/userController');
const authController = require('./controller/AuthController');
const authLoginController = require('./controller/AuthLoginController');
const accountController = require('./controller/accountController');
const movementController = require('./controller/movementController');
const movementListController = require('./controller/movementListController');

/**
Los app.xxx() tienen que estar ordenados de más genérico a más específico
para que el registro de rutas se haga bien.
**/

//GET USERS + BY ID
app.get('/apitechu/v2/users', userController.getUsersV2);
app.get('/apitechu/v2/users/:id', userController.getUsersByV2);
// POST USERS
app.post('/apitechu/v2/users', userController.createUsersV2);
//LOG (IN-OUT)
app.post('/apitechu/v2/users/login', authController.login);
//app.post('/apitechu/v2/users/login', authLoginController.login);
app.post('/apitechu/v2/users/logout/:id', authController.logout);

//GET ACCOUNT
app.get('/apitechu/v2/accounts', accountController.getAccounts);
//app.get('/apitechu/v2/accounts/balance/', movementController.getAccBalance);
//GET MOVEMENT
app.get('/apitechu/v2/movements', movementController.getMovements);
app.get('/apitechu/v2/accounts/movements', movementListController.getMovementsList);

// POST ACCOUNT + MOVEMENT
app.post('/apitechu/v2/accounts', accountController.createAccounts);
app.post('/apitechu/v2/accounts/movements', movementController.createMovements);






//**********************************************//
//**********************************************//
// GET HELLO registro de ruta de API
app.get('/apitechu/v1/hello',
  function(req, res){
    console.log("GET /apitechu/v1/hello");
    res.send({"msg": "Hola desde APITechU !! (:-P)  "});
  }
);

//POST MONSTRUO
app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req, res){
   console.log("POST /apitechu/v1/monstruo/:p1/p2");

    console.log("Parámetros");
    console.log(req.params);

    console.log("Query");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
);
