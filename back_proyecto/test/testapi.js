const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();
var server = require('../server'); //con esto iniciamos la Aplicacion de forma automatica.
                                   // al acabar el test lo cierra.

describe('Fist unit test',
  function() {
    it('Test that Duckduckgo works', function(done) {
        chai.request('http://www.duckduckgo.com')
        //chai.request('https://developer.mozilla.org/en-US/adsfasdfas')
        .get('/')
        .end(
          function(err, res) {
            console.log("Request has finshed");
            //console.log(err);
            //console.log(res);
            //esta es la aserción a comprobar.
            res.should.have.status(200); //el resultado debe dar 200
            // OJO!!! es necesario probar el negativo
            done();
          }
        )
      }
    )
  }
)

describe('Test de API Usuarios', //al meter esta prueba, en el anterior metemos de nuevo arriba el duckduck.
  function() {
    it('Test that HOLA works', function(done) {
        chai.request('http://localhost:3000')
        .get('/apitechu/v1/hello')
        .end(
          function(err, res) {
            console.log("Request has finshed");
            res.should.have.status(200); //el resultado debe dar 200
            done(); //si no le ponemos el done, llegaria a dar el timeout esperando. Hay que cerrar la prueba.
          }
        )
      }
    ),
    it('Prueba que la API devuelve una lista de usuarios correcta', function(done) {
        chai.request('http://localhost:3000')
        .get('/apitechu/v1/users')
        .end(
          function(err, res) {
            console.log("Request has finshed");
            res.should.have.status(200); //el resultado debe dar 200
            //Por cada test debería haber una aserción o comprobación
            res.body.users.should.be.a("array");
            for (user of res.body.users) {
              user.should.have.property('email');
              user.should.have.property('pasword');
            }
            done(); //si no le ponemos el done, llegaria a dar el timeout esperando. Hay que cerrar la prueba.
          }
        )
      }
    )
  }
)
